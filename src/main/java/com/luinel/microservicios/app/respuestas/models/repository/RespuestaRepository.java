 package com.luinel.microservicios.app.respuestas.models.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.luinel.microservicios.app.respuestas.models.entity.Respuesta;

public interface RespuestaRepository extends MongoRepository<Respuesta, String> {

	@Query("{'alumnoId': ?0, 'preguntaId': { $in: ?1 }}")
	public Iterable<Respuesta> findRespuestaByAlumnoByPreguntasIds(Long alumnoId, Iterable<Long> preguntasIds);
	
	@Query("{'alumnoId': ?0}")
	public Iterable<Respuesta> findByAlumnoId(Long alumnoId);
	
	@Query("{'alumnoId': ?0, 'pregunta.examen.id': ?1}")
	public List<Respuesta> findRespuestaByAlumnoByExamen(Long alumnoId, Long examenId);
	
	@Query(value = "{'alumnoId': ?0}", fields = "{'pregunta.examen.id': 1}" )
	public Iterable<Respuesta> findExamenesIdConRespuestasByAlumno(Long alumnoId);
	
}
